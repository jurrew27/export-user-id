import sys
import json
import requests
import os

def _main(file, from_line = None, to_line = None):
    with open(file) as f:
        f_results = open('results.txt', 'a')
        
        for line_number, line in enumerate(f):
            if from_line and line_number < from_line:
                continue
            if to_line and  line_number >= to_line:
                return
                
                
            user = json.loads(line)
            user['id'] = user['_id']['$oid']
            
            r = requests.get(
                'https://app.hellolily.com/api/contacts/',
                headers={'Authorization': 'Token {}'.format(os.environ['LILY_API_KEY'])},
                params={'search': 'email_address:' + user['email']}
            )
            if not r.ok:
                f_results.write('line {} error\n'.format(line_number))
                print('Error on file line {}: '.format(line_number))
                r.raise_for_status()
                
            body = r.json()
            
            if len(body['results']) == 1:
                r = requests.patch(
                    'https://app.hellolily.com/api/contacts/' + str(body['results'][0]['id']) + '/',
                    headers={'Authorization': 'Token {}'.format(os.environ['LILY_API_KEY'])},
                    json={'description': 'User id: ' + str(user['id'])}
                )
                if not r.ok:
                    f_results.write('line {} error\n'.format(line_number))
                    print('Error on file line {}: '.format(line_number))
                    r_results.raise_for_status()
                
                f_results.write('line {} successful\n'.format(line_number))
            else:
                f_results.write('line {} attempted\n'.format(line_number))
                
        f_results.close()

if __name__ == '__main__':
    if len(sys.argv) < 1 or sys.argv[1] == '--help' or sys.argv[1] == '-h':
        print('Usage: lily_contacts.py file [from_line] [to_line]')
    elif len(sys.argv) == 4:
        _main(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]))
    else:
        _main(sys.argv[1])
