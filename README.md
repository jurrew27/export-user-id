Posts all user IDs to their corresponding Lily contacts, found by email address. 

## Requirements 
- Python 3
- Requests
- Asyncio (for async version)
- Aiohttp (for async version)

Install using ```pip3 install <package>```

## Usage
```lily_contacts.py file [from_line] [to_line]```

Make sure to have the Lily API token in the environment variable ```LILY_API_KEY```. 
Results will be written to ```results.txt``` in the same directory. 